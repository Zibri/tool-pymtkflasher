# MT6261/MT2503 Flash Tool
Python based flash tool for Mediatek MT6261 and MT2503 SoC.

## Credits
Georgi Angelov (@Wiz-IO) for initial work  
Anton Rieckert (@alrieckert) for pre-format fix
